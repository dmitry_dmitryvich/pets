package ru.bokov;

/**
 * Демо класс, демонстрирующий класс pets
 *
 * @author Боков Дмитрий
 */
public class Main {
    public static void main(String[] args){
        Cat sonya = new Cat();
        Horse angel = new Horse();
        Snake gaduka = new Snake();
        Frog mazik = new Frog();
        sonya.move();
        sonya.talk();
        sonya.move();
        angel.move();
        angel.talk();
        angel.move();
        gaduka.move();
        gaduka.talk();
        gaduka.move();
        mazik.move();
        mazik.talk();
        mazik.move();
    }
}

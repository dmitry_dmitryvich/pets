package ru.bokov;

/**
 * Абстрактный класс
 *
 * @author Боков Дмитрий
 */
public abstract class Pets {
    /**
     * progress - расстояние, преодолённое животным
     * move() - метод, задающий движение животного
     * talk() - метод, задающий голос животного
     */
    protected double progress;
    abstract void move();
    abstract void talk();
}

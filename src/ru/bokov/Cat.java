package ru.bokov;

/**
 * Класс кошка
 *
 * @author Боков Дмитрий
 */
public class Cat extends Pets {
    @Override
    void move() {
        progress += 1;
        System.out.println("Кошка крадётся " + ':' + +progress + " м.");
    }

    @Override
    void talk() {
        System.out.println("Соня говорит 'мяу-мяу' ");
    }
}
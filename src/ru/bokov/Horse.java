package ru.bokov;

/**
 * Класс лошадка
 *
 * @author Боков Дмитрий
 */
public class Horse extends Pets {
    @Override
    void move() {
        progress += 10;
        System.out.println("Лошадка скачет " +  ':' +  +progress + " м.");
    }

    @Override
    void talk() {
        System.out.println("Ангел говорит 'и-го-го' ");
    }
}
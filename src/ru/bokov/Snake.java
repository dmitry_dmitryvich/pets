package ru.bokov;

/**
 * Класс змея
 *
 * @author Боков Дмитрий
 */
public class Snake extends Pets {
    @Override
    void move(){
        progress +=0.3;
        System.out.println("Гадюка ползёт " + ':' + progress + " м.");
    }

    @Override
    void talk(){
        System.out.println("Гадюка говорит 'ш-ш-ш' ");
    }
}

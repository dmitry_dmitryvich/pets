package ru.bokov;

/**
 * Класс лягушка
 *
 * @author Боков Дмитрий
 */
public class Frog extends Pets{
    @Override
    void move() {
        progress += 2;
        System.out.println("Лягушка прыгает "  + ':' + +progress + " м.");
    }

    @Override
    void talk() {
        System.out.println("Мазик говорит 'ква-ква' ");
    }
}